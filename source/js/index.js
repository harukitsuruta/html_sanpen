// UserAgent
var _ua = (function(u){
  var mobile = {
            0: (u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
            || u.indexOf("iphone") != -1
            || u.indexOf("ipod") != -1
            || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
            || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
            || u.indexOf("blackberry") != -1,
            iPhone: (u.indexOf("iphone") != -1),
            Android: (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
  };
  var tablet = (u.indexOf("windows") != -1 && u.indexOf("touch") != -1)
            || u.indexOf("ipad") != -1
            || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
            || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
            || u.indexOf("kindle") != -1
            || u.indexOf("silk") != -1
            || u.indexOf("playbook") != -1;
  var pc = !mobile[0] && !tablet;
  return {
    Mobile: mobile,
    Tablet: tablet,
    PC: pc
  };
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet) {
  var width = 1080;
  document.getElementsByName('viewport')[0].setAttribute('content', 'width=' + width + ',initial-scale=1');
}

$(function(){

  // lazyload
  $('img.lazy').lazyload({
    effect: 'fadeIn',
    effectspeed: 1000,
    threshold: 10000
  });

  // スマホ
  if($('#header').hasClass('spheader')) {
    // menu open(toggle)
    $('.js-menuOpen').on('click', function(){
      $('.js-sidemenu').toggleClass('is_active');
    });
    $('.js-menuClose').on('click', function(){
      $('.js-sidemenu').removeClass('is_active');
    });
    // search
    $('.js-searchToggle').on('click', function(){
      $('.js-searchInput').toggleClass('is_active');
    });
    // nav
    $('.js-navList li').on('click', function(e){
      event.preventDefault(e);
      var i = $('.js-navList li').index(this);
      if(!$(this).hasClass('is_active')) {
        $('.js-navList li').removeClass('is_active');
        $(this).addClass('is_active');
        $('.navcontent ul').removeClass('is_active');
        $('.navcontent ul').eq(i).addClass('is_active');
      }else{
        $(this).removeClass('is_active');
        $('.navcontent ul').removeClass('is_active');
      }
    });
  }

  // アンカーリンク
  $('a[href^="#"]').click(function() {
    var speed = 800;
    var easing = 'swing';
    var href = $(this).attr('href');
    var target = $(href == '#' || href == '' ? 'html' : href);
    var position = target.offset().top;
    $('html, body').animate({
      scrollTop: position
    }, speed, easing);
    return false;
  });

  // タブ
  $('.tab__list .tab__item').click(function() {
    $('.tab__item.is_current').removeClass('is_current');
    $(this).addClass('is_current');
    $('.is_show').removeClass('is_show');
    // クリックしたタブからインデックス番号を取得
    var index = $(this).index();
    // クリックしたタブと同じインデックス番号をもつコンテンツを表示
    $('.tab__content').eq(index).addClass('is_show');
  });

  //pagetop
  var pagetop = $('.pagetop');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      pagetop.fadeIn();
    } else {
      pagetop.fadeOut();
    }
  });
  pagetop.click(function () {
    $('body, html').animate({ scrollTop: 0 }, 500);
    return false;
  });

  //タブ切り替え
  $('.tabs').on('click', function(){
    $('.tab .tabs').removeClass('is_current');
    $(this).addClass('is_current');
    $('.tab .tab__item').removeClass('is_active');
    var i = $(this).index();
    $('.tab .tab__item').eq(i).addClass('is_active');
  });

  //動画サムネイル非表示
  $('.tab .tab__item .ph').on('click', function(){
    $(this).children('img').hide();
  });

});