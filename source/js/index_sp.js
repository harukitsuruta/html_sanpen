
var swiper = new Swiper('.swiper-container', {
  effect: 'fade',
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 3000,
  },
  speed: 2000,
  pagination: {
    el: '.swiper-pagination',
  },
});