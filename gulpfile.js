// import plugin
const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const cssnext = require('postcss-cssnext');
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
//const webpackStream = require('webpack-stream');
//const webpack = require('webpack');
//const webpackConfig = require('./webpack.config');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');  // 圧縮率を高めるのにプラグインを入れる png
const mozjpeg = require('imagemin-mozjpeg');  // 圧縮率を高めるのにプラグインを入れる jpg
const changed = require('gulp-changed');
const notify = require('gulp-notify');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

/****************************
path
****************************/
const path = {
  'src': './source/',
  'dist': './asset/'
}

/****************************
browserSync / auto reload
****************************/
gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: './'
    },
    port: 8000,
    open: false
  })
});
function reload(done) {
  browserSync.reload();
  done()
}

/****************************
gulp image minify
****************************/
gulp.task('images', () => {
  return gulp.src(path.src + '/**/*.{png,jpg,gif,svg}')
    .pipe(changed(path.dist))  // src と dist を比較して異なるものだけ処理
    .pipe(imagemin([
      pngquant({
        quality: '65-80',  // 画質
        speed: 1,  // 最低のスピード
        floyd: 0,  // ディザリングなし
      }),
      mozjpeg({
        quality: 85, // 画質
        progressive: true
      }),
      imagemin.svgo(),
      imagemin.optipng(),
      imagemin.gifsicle()
    ]))
    .pipe(gulp.dest(path.dist))  // 保存
    .pipe(notify('&#x1f363; images task finished &#x1f363;'));
});

/****************************
js
****************************/
gulp.task('js', () => {
  return (
    gulp
      .src(path.src + 'js/**/*.js')
      .pipe(plumber())
      .pipe(uglify())
      .pipe(rename({extname: '.min.js'}))
      .pipe(gulp.dest(path.dist + 'js/'))
  )
});

/****************************
gulp sass
****************************/
gulp.task('sass', () => {
  const processors = [
    cssnext({browsers:['last 2 version', 'iOS >= 8.1', 'Android >= 4.4']})
  ];
  return (
    gulp
      .src(path.src + 'scss/**/*.scss')
      .pipe(plumber())
      //.pipe(sourcemaps.init())
      .pipe(sass())
      .on('error', (err) => {
        console.log(err.message)
      })
      .pipe(postcss(processors))
      .pipe(sass({
        outputStyle: 'compact'//nested(ネストがインデントされる)、compact（規則集合毎が1行になる）、compressed（全CSSコードが1行になる）
      }))
      //.pipe(sourcemaps.write())
      .pipe(gulp.dest(path.dist + 'css/'))
  );
});

/****************************
gulp pug
****************************/
gulp.task('pug', function () {
  return(
	gulp
    .src([path.src + 'pug/**/*.pug', '!' + path.src + 'pug/_var/**/*.pug'])
		.pipe(
      pug({
        pretty: '\t',
        basedir: path.src + 'pug'
      })
		)
		.pipe(gulp.dest('./'))
  );
});

/****************************
watch
****************************/
gulp.task('watch', () => {
  gulp.watch(path.src + 'img/**/*.{png,jpg,gif,svg}', gulp.series('images', reload));
  gulp.watch(path.src + 'scss/**/*.scss', gulp.series('sass', reload));
  gulp.watch(path.src + 'pug/**/*.pug', gulp.series('pug', reload));
  gulp.watch(path.src + 'js/**/*.js', gulp.series('js', reload));
  //gulp.watch(path.src + 'js/**/*.js', gulp.series('bundle', reload));
});

/****************************
default
****************************/
const defaultTasks = gulp.parallel('watch','browser-sync');
gulp.task('default', defaultTasks, (done) => {
  done();
});